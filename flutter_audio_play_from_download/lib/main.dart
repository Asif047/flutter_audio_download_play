import 'dart:io';

import 'package:audioplayers/audioplayers.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:path_provider/path_provider.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key});


  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

   AudioPlayer audioPlayer = AudioPlayer();

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(
        title: const Text("File Download"),
      ),
      body: Center(

        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children:  <Widget>[
            const Text(
              'Hello world',
            ),

            ElevatedButton(
              onPressed: () async {
                Dio dio = Dio();
                createDirectory();
                
                try {
                  var dir = await getApplicationDocumentsDirectory();
                  var res = await dio.download(
                      "https://file-examples.com/storage/fe332cf53a63a4bd5991eb4/2017/11/file_example_MP3_700KB.mp3", "${dir.path}/audio/a.mp3",
                      onReceiveProgress: (rec, total) {
                        print("Rec: $rec , Total: $total");

                        setState(() {

                        });
                      });

                } catch (e) {
                  print(e);
                }

              },
              child:  Text('Download'),
            ),


            ElevatedButton(
              onPressed: () async {

                print("CLICKED");

                try {

                  var dir = await getApplicationDocumentsDirectory();
                  await audioPlayer.play(DeviceFileSource("${dir.path}/audio/a.mp3"));

                } catch (e) {
                  print("##ERROR:"+e.toString());
                }

              },
              child:  Text('Play'),
            ),
          ],
        ),
      ),

    );
  }



  void createDirectory() async {
    final dir2 =
    Directory((await getApplicationDocumentsDirectory()).path + "/files");
    if ((await dir2.exists())) {
    } else {
      dir2.create();
    }

  }




}
